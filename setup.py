import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="autobotmaze",
    version="0.0.2",
    author="mark bracher",
    author_email="bracher@gmail.com",
    description="Automatic maze creation for a bot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/autobotmaze",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'Cython',
        'numpy',
        'pygame',
        'pynanosvg'
    ]
)
