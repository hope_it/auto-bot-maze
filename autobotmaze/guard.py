from random import randint

class Guard:
    position = None
    last_move = None
    inverses = {
        'N': 'S',
        'S': 'N',
        'E': 'W',
        'W': 'E'
    }
    
    def __init__(self, position):
        self.position = position

    def move(self):
        moves = []
        for d in self.inverses.keys():
            if self.last_move and d == self.inverses[self.last_move]:
                # skip
                continue
            if self.position.valid(d):
                moves.append(d)
        # only available move is to turn around
        if len(moves) == 0:
            moves.append(self.inverses[self.last_move])
        
        # randomly pick one of the valid moves we selected above
        move = moves[randint(0, len(moves)-1)]
        # and populate last_move for next time
        self.last_move = move
        # change position
        self.position.move(move)
