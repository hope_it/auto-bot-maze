#!/usr/bin/env python3

import random
import time
from autobotmaze import Runner

# note:  don't call it 'random.py', bad things will happen with imports

def callback(position):
    # sleep for a bit, just to avoid flogging the cpu
    time.sleep(0.2)
    choice = random.choice(['N', 'S', 'E', 'W'])
    return choice


if __name__ == '__main__':
    app = Runner(callback)
    app.run()
